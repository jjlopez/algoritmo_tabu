package com.dp1mtel.algo.loader;

import com.dp1mtel.algo.model.Problem;

import java.util.List;

public interface Loader {
    List<Problem> load();
}
