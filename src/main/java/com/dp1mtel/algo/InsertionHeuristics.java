package com.dp1mtel.algo;

import com.dp1mtel.algo.model.Customer;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.*;
import java.util.stream.IntStream;

public class InsertionHeuristics implements Solver {
    private final static double[][] PARAMETERS = new double[][]{
            {1, 1, 1, 0},
            {1, 2, 1, 0},
            {1, 1, 0, 1},
            {1, 2, 0, 1},
            {1, 1, .5, .5},
            {1, 2, .5, .5},
    };

    @Override
    public String getName() {
        return "INSERTION HEURISTICS";
    }

    @Override
    public Optional<Solution> solve(Problem problem) {
        List<int[]> bestRoutes = null;
        double lowestCost = Double.POSITIVE_INFINITY;

        for (double[] param : PARAMETERS) {
            Set<Customer> customers = new HashSet<>(problem.getCustomers());
            List<int[]> routes = new ArrayList<>();
            // Iterate until all customers are assigned to a route
            while (!customers.isEmpty()) {
                RouteInsertion insertion = new RouteInsertion(
                        problem,
                        customers,
                        param[0],
                        param[1],
                        param[2],
                        param[3]);
                insertion.solve();
                routes.add(insertion.getRoute());
            }
            double cost = objectiveFunction(problem, routes);
            if (cost < lowestCost) {
                lowestCost = cost;
                bestRoutes = routes;
            }
        }

        return Optional.of(toSolution(bestRoutes, problem));
    }

    private double objectiveFunction(Problem problem, List<int[]> routes) {
        return problem.objectiveFunction(routes);
    }

    private Solution toSolution(List<int[]> bestRoutes, Problem problem) {
        List<Route> routes = new ArrayList<>();
        List<Customer> customers = problem.getCustomers();

        for (int[] r : bestRoutes) {
            List<Customer> cls = new ArrayList<>();
            IntStream.range(1, r.length - 1).forEach((i) -> {
                cls.add(customers.get(r[i] - 1));
            });
            routes.add(new Route(cls));
        }

        return new Solution(problem, routes);
    }

    private class RouteInsertion {
        private final double[][] distance;
        private final Problem problem;
        private final Set<Customer> customers;
        private final TDFunction tdFunction;
        private final double my;
        private final double lambda;
        private final double alpha1;
        private final double alpha2;
        private int[] route = new int[]{ 0, 0 };

        public RouteInsertion(Problem problem,
                              Set<Customer> customers,
                              double my,
                              double lambda,
                              double alpha1,
                              double alpha2) {
            this.problem = problem;
            this.tdFunction = problem.getTDFunction();
            this.customers = customers;
            this.my = my;
            this.lambda = lambda;
            this.alpha1 = alpha1;
            this.alpha2 = alpha2;
            distance = problem.distanceMatrix();
        }

        public Set<Customer> getCustomers() {
            return customers;
        }

        public int[] getRoute() {
            return route;
        }

        public void solve() {
            while (!customers.isEmpty()) {
                Map<Customer, Integer> positions = new HashMap<>();
                customers.forEach((c) -> {
                    double leastC1 = Double.POSITIVE_INFINITY;
                    for (int p = 1; p < route.length; p++) {
                        double c1 = c1(route[p - 1], c.getNumber(), route[p]);
                        int[] potentialRoute = insert(route, p, c.getNumber());
                        if (problem.feasible(potentialRoute) && leastC1 > c1) {
                            positions.put(c, p);
                            leastC1 = c1;
                        }
                    }
                });
                double optimum = Double.NEGATIVE_INFINITY;
                Customer ci = null;
                for (Map.Entry<Customer, Integer> entry : positions.entrySet()) {
                    int p = entry.getValue();
                    int u = entry.getKey().getNumber();
                    double c2 = c2(route[p - 1], u, route[p]);
                    if (c2 < optimum) {
                        ci = entry.getKey();
                        optimum = c2;
                    }
                }

                if (ci == null) {
                    break;
                } else {
                    route = insert(route, positions.get(ci), ci.getNumber());
                    customers.remove(ci);
                }
            }
        }

        private double c1(int i, int u, int j) {
            return alpha1 * c11(i, u, j) + alpha2 * c12(i, u, j);
        }

        private double c2(int i, int u, int j) {
            return lambda * distance[0][u] - c1(i, u, j);
        }

        private double c11(int i, int u, int j) {
            return distance[i][u] + distance[u][j] - my * distance[i][j];
        }

        private double c12(int i, int u, int j) {
            int p = p(j);
            return b(insert(route, p, u), p + 1) - b(route, p);
        }

        private int[] insert(int[] r, int p, int c) {
            int[] result = Arrays.copyOf(r, r.length + 1);
            result[p] = c;
            for (int i = p; i < r.length; i++) {
                result[i + 1] = r[i];
            }
            return result;
        }

        private double b(int[] r, int p) {
            if (r.length == 2) {
                return 0;
            }

            List<Customer> allCustomers = problem.getCustomers();
            double time = 0;
            int position = 0;
            for (int i = 1; i <= p; i++) {
                time += tdFunction.travelTime(position, r[i], time);
                position = r[i];
                if (position == 0) {
                    return time;
                }
                Customer c = allCustomers.get(position - 1);
                time = Math.max(c.getReadyTime(), time);
                if (i == p) {
                    return time;
                }
                time += c.getServiceTime();
            }

            return -1;
        }

        private int p(int c) {
            if (c == 0) {
                return route.length - 1;
            }

            for (int i = 0; i < route.length; i++) {
                if (route[i] == c) {
                    return i;
                }
            }
            return -1;
        }
    }
}
