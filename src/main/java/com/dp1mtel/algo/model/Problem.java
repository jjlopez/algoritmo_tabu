package com.dp1mtel.algo.model;

import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.List;
import java.util.stream.Collectors;

public interface Problem {

    /**
     * Returns the problem's time dependent function
     *
     * @return time dependent function
     */
    TDFunction getTDFunction();

    /**
     * Sets the problem's time dependent function
     *
     * @param tdFunction New time dependent function
     */
    void setTDFunction(TDFunction tdFunction);

    /**
     * Returns the name of the problem instance.
     *
     * @return name of problem instance
     */
    String getName();

    /**
     * Returns the depot.
     *
     * @return the depot
     */
    Depot getDepot();

    /**
     * Returns a list of customers.
     *
     * @return list of customers
     */
    List<Customer> getCustomers();

    /**
     * Returns a list of nodes
     *
     * @return list of nodes
     */
    List<Node> getNodes();

    /**
     * Returns the number of available vehicles.
     *
     * @return the number of vehicles
     */
    int getAvailableVehicles();

    /**
     * Returns the capacity of a single vehicle.
     * Assumes homogeneous vehicles
     *
     * @return the capacity
     */
    int getVehicleCapacity();

    /**
     * Calculates a distance matrix of the instance.
     *
     * @return distance matrix
     */
    default double[][] distanceMatrix() {
        final List<Customer> customers = getCustomers();
        final Depot depot = getDepot();

        int n = customers.size() + 1;
        double[][] matrix = new double[n][n];
        customers.forEach((cA) -> {
            customers.forEach((cB) -> {
                matrix[cA.getNumber()][cB.getNumber()] = cA.getPosition().distance(cB.getPosition());
            });
        });

        matrix[depot.getNumber()][depot.getNumber()] = 0;
        customers.forEach((customer) -> {
            double distance = depot.getPosition().distance(customer.getPosition());
            matrix[depot.getNumber()][customer.getNumber()] = distance;
            matrix[customer.getNumber()][depot.getNumber()] = distance;
        });

        return matrix;
    }

    default boolean feasible(int[] r) {
        TDFunction tdFunction = getTDFunction();
        List<Customer> allCustomers = getCustomers();
        int remainingCapacity = getVehicleCapacity();
        double time = 0;
        int position = 0;

        for (int i = 1; i < r.length; i++) {
            time += tdFunction.travelTime(position, r[i], time);
            position = r[i];
            Customer c = allCustomers.get(position - 1);
            time = Math.max(c.getReadyTime(), time);
            if (time > c.getDueTime()) {
                return false;
            }

            time += c.getServiceTime();
            remainingCapacity -= c.getDemand();
            if (remainingCapacity < 0) {
                return false;
            }
        }

        time += tdFunction.travelTime(r[r.length - 2], 0, time);
        return time <= getDepot().getClosingTime();
    }

    default boolean feasible(Route r) {
        return feasible(r.toIntArr());
    }

    default double objectiveFunction(List<int[]> routes) {
        double obj = 0;

        for (int[] r : routes) {
            obj += assumedTravelTime(r);
        }

        return obj;
    }

//    default double objectiveFunction(List<Route> routes) {
//        return objectiveFunction(routes.stream().map(Route::toIntArr).collect(Collectors.toList()));
//    }

    default double objectiveFunction(Solution solution) {
        return objectiveFunction(solution.getRoutes().stream().map(Route::toIntArr).collect(Collectors.toList()));
    }

    default double assumedTravelTime(int[] route) {
        TDFunction tdFunction = getTDFunction();

        double departureTime = 0;
        double totalTravelTime = 0;
        List<Node> nodes = getNodes();
        int position = 0;

        /**
         * From `1` to `route.length - 1` because first
         * and last nodes are the depot
         */
        for (int i = 1; i < route.length - 1; i++) {
            int nodeId = route[i];
            double travelTime = tdFunction.travelTime(position, nodeId, departureTime);
            totalTravelTime += travelTime;
            double arrivalTime = departureTime + travelTime;
            position = nodeId;
            Node node = nodes.get(position - 1);
            if (node instanceof Customer) {
                Customer c = ((Customer) node);
                departureTime = Math.max(arrivalTime, c.getReadyTime()) + c.getServiceTime();
            } else {
                departureTime = arrivalTime;
            }
        }

        return totalTravelTime;
    }

    default double assumedTravelTime(Route route) {
        return assumedTravelTime(route.toIntArr());
    }
}