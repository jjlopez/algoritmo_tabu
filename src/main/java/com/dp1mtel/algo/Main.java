package com.dp1mtel.algo;

import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;
import com.dp1mtel.algo.solver.strategy.SimpleSolutionLocator;
import com.dp1mtel.algo.tabu.TabuParameters;
import com.dp1mtel.algo.tabu.TabuSolver;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.Optional;

public class Main {
    private Problem problem;
    private TDFunction tdFunction;

    public static void main(String[] args) {
        Main main = new Main();
        Optional<Solution> solution = main.tabu();
    }

    private Optional<Solution> tabu() {
        TabuParameters tabuParameters = new TabuParameters(
                1000,
                100,
                60000,
                10);
        SimpleSolutionLocator solutionLocator = new SimpleSolutionLocator(problem);

        Solver solver = new TabuSolver(solutionLocator, tabuParameters);
        return solver.solve(problem);
    }
}