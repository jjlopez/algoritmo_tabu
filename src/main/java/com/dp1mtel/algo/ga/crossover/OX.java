package com.dp1mtel.algo.ga.crossover;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.ChromosomePair;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.solver.Route;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class OX implements Crossover {

    private Random random = ThreadLocalRandom.current();

    @Override
    public ChromosomePair cross(Chromosome parent1, Chromosome parent2) {
        int length = parent1.getRoute().size();
        int rA = random.nextInt(length);
        int rB = random.nextInt(length);

        if (rA == rB) {
            rB = (rB + 1) % length;
        }
        return cross(parent1, parent2, rA, rB);
    }

    private ChromosomePair cross(Chromosome parent1, Chromosome parent2, int rA, int rB) {
        int len = parent1.getRoute().size();

        int csA = Math.min(rA, rB);
        int csB = Math.max(rA, rB);

        Route c1 = parent1.getRoute().subRoute(csA, csB + 1);
        Route c2 = parent2.getRoute().subRoute(csA, csB + 1);

        Route t1 = parent1.getRoute().subRoute(csA, csB + 1);
        Route t2 = parent2.getRoute().subRoute(csA, csB + 1);
        t1.getRoute().sort(Comparator.comparingInt(Node::getNumber));
        t2.getRoute().sort(Comparator.comparingInt(Node::getNumber));

        int iA = csB;
        int iB = csB;

        for (int i = csB + 1; i < len; i++) {
            if (Collections.binarySearch(t2.getRoute(), parent1.getRoute().get(i)) < 0) {
                iB = (iB + 1) % len;
                c2.set(iB, parent1.getRoute().get(i));
            }
            if (Collections.binarySearch(t1.getRoute(), parent2.getRoute().get(i)) < 0) {
                iA = (iA + 1) % len;
                c1.set(iA, parent2.getRoute().get(i));
            }
        }

        for (int i = 0; i <= csB; i++) {
            if (Collections.binarySearch(t2.getRoute(), parent1.getRoute().get(i)) < 0) {
                iB = (iB + 1) % len;
                c2.set(iB, parent1.getRoute().get(i));
            }
            if (Collections.binarySearch(t1.getRoute(), parent2.getRoute().get(i)) < 0) {
                iA = (iA + 1) % len;
                c1.set(iA, parent2.getRoute().get(i));
            }
        }

        return new ChromosomePair(parent1.copy(c1), parent2.copy(c2));
    }

}
