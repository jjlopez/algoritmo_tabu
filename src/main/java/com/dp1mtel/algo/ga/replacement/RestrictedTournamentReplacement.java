package com.dp1mtel.algo.ga.replacement;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.Collection;

public class RestrictedTournamentReplacement implements Replacement {
    @Override
    public void replace(Population population, Collection<Chromosome> children) {
        children.forEach((child) -> {
            int distance = Integer.MAX_VALUE;
            Chromosome similarParent = null;
            for (Chromosome parent : population.getChromosomes()) {
                int d = child.hammingDistance(parent);
                if (d < distance) {
                    distance = d;
                    similarParent = parent;
                }
            }
            if (similarParent != null && similarParent.fitness() < child.fitness()) {
                if (population.remove(similarParent)) {
                    population.add(child);
                }
            }
        });
    }
}
