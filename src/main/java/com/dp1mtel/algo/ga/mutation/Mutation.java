package com.dp1mtel.algo.ga.mutation;

import com.dp1mtel.algo.ga.Chromosome;

public interface Mutation {
    void mutate(Chromosome chromosome);
}
