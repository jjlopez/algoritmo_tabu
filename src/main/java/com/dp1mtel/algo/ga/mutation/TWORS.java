package com.dp1mtel.algo.ga.mutation;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.solver.Route;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Swaps two randomly chosen genes
 */
public class TWORS implements Mutation {
    private Random random = ThreadLocalRandom.current();

    @Override
    public void mutate(Chromosome chromosome) {
        Route route = chromosome.getRoute();
        int len = route.size();

        int iA = random.nextInt(len);
        int iB = random.nextInt(len);
        if (iA == iB) {
            iB = (iB + 1) % len;
        }

        route.swap(iA, iB);
    }
}
