package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.InsertionHeuristics;
import com.dp1mtel.algo.ga.mutation.TWORS;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class PopulationCreator {
    private static final Random random = ThreadLocalRandom.current();
    private static final TWORS twors = new TWORS();

    public static Population createPopulation(Problem problem, GAParameters parameters) {
        List<Chromosome> chromosomes = new ArrayList<>(parameters.populationSize());

        addSolution(new InsertionHeuristics(), problem, parameters, chromosomes);

        expandPopulation(chromosomes, parameters);

        return new Population(chromosomes);
    }

    private static void addSolution(Solver solver,
                                    Problem problem,
                                    GAParameters parameters,
                                    List<Chromosome> chromosomes) {
        solver.solve(problem)
                .map((solution) -> transformToChromosome(solution, parameters))
                .ifPresent(chromosomes::add);
    }

    private static void expandPopulation(List<Chromosome> chromosomes, GAParameters parameters) {
        while (chromosomes.size() < parameters.populationSize()) {
            Chromosome parent = chromosomes.get(random.nextInt(chromosomes.size()));
            Chromosome child = parent.copy(parent.getRoute());
            int mutations = random.nextInt(10);
            for (int i = 0; i < mutations; i++) {
                twors.mutate(child);
            }
            chromosomes.add(child);
        }
    }

    private static Chromosome transformToChromosome(Solution solution, GAParameters parameters) {
        List<Node> nodes = new ArrayList<>();
        Collection<Route> routes = solution.getRoutes();

        routes.forEach((r) -> nodes.addAll(r.getNodes()));

        return new Chromosome(solution.getProblem(), parameters, new Route(nodes), 1);
    }
}
