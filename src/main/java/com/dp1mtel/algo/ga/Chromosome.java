package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.ga.splitter.Splitter;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Chromosome {
    private Problem problem;
    private GAParameters parameters;
    private Route route;
    private int generation;
    private double fitness = -1;

//    static Random random = new Random(GASolver.RANDOM_SEED);
    static Random random = ThreadLocalRandom.current();

    public Chromosome(Problem problem, GAParameters parameters, Route route, int generation) {
        this.problem = problem;
        this.parameters = parameters;
        this.route = route;
        this.generation = generation;
    }

    public double fitness() {
        if (fitness < -1) {
            List<Route> subRoutes = parameters.splitter().split(this);
            double totalCost = problem.objectiveFunction(subRoutes.stream()
                    .map(Route::toIntArr)
                    .collect(Collectors.toList()));
            fitness = 1.0 / totalCost;
        }
        return fitness;
    }

    public int hammingDistance(Chromosome other) {
        int distance = 0;
        for (int i = 0; i < route.size(); i++) {
            if (route.get(i).getNumber() != other.route.get(i).getNumber()) {
                distance++;
            }
        }
        return distance;
    }

    public Problem getProblem() {
        return problem;
    }

    public int getGeneration() {
        return generation;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
        this.fitness = -1;
    }

    public List<Route> routes(Splitter splitter) {
        List<Node> allNodes = problem.getNodes();
        List<Route> routes = new ArrayList<>();
        for (Route route : splitter.split(this)) {
            List<Node> nodes = new ArrayList<>();
            for (Node node : route.getRoute()) {
                nodes.add(allNodes.get(node.getNumber() - 1));
            }
            routes.add(new Route(nodes));
        }
        return routes;
    }

    public Chromosome cross(Chromosome other) {
        int crossoverMethod = random.nextInt(parameters.crossovers().length);
        ChromosomePair pair = parameters.crossovers()[crossoverMethod].cross(this, other);

        if (pair.left().fitness() > pair.right().fitness()) {
            return pair.left();
        }
        return pair.right();
    }

    public void mutate() {
        int mutationMethod = random.nextInt(parameters.mutations().length);
        parameters.mutations()[mutationMethod].mutate(this);
        fitness = -1;
    }

    public Chromosome getChild() {
        Chromosome child = new Chromosome(problem, parameters, new Route(route), generation + 1);
        child.fitness = fitness;
        return child;
    }

    public Chromosome copy(Route route) {
        return new Chromosome(problem, parameters, route, generation);
    }

    public Chromosome copy() {
        return new Chromosome(problem, parameters, route, generation);
    }
}
