package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.utils.Pair;

public class ChromosomePair extends Pair<Chromosome, Chromosome> {
    public ChromosomePair(Chromosome left, Chromosome right) {
        super(left, right);
    }
}
