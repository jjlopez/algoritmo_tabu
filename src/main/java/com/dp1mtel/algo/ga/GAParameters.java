package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.ga.crossover.Crossover;
import com.dp1mtel.algo.ga.crossover.OX;
import com.dp1mtel.algo.ga.mutation.TWORS;
import com.dp1mtel.algo.ga.mutation.Mutation;
import com.dp1mtel.algo.ga.replacement.Replacement;
import com.dp1mtel.algo.ga.replacement.RestrictedTournamentReplacement;
import com.dp1mtel.algo.ga.selection.RouletteSelection;
import com.dp1mtel.algo.ga.selection.Selection;
import com.dp1mtel.algo.ga.splitter.Splitter;
import com.dp1mtel.algo.ga.splitter.StraightSplitter;

public class GAParameters {
    private int populationSize = 30;

    private int maxRounds = 100000;
    private int maxRoundsWithoutImproving = 2000;
    private double selectionRate = .4;
    private double mutationProbability = 0.3;

    private Selection selection = new RouletteSelection();
    private Replacement replacement = new RestrictedTournamentReplacement();
    private Splitter splitter = new StraightSplitter();

    private Mutation[] mutations = new Mutation[]{/*new CIM(), new RSM(), */new TWORS()};
    private Crossover[] crossovers = new Crossover[]{new OX()/*, new NWOX(), new CX()*/};

    public int populationSize() {
        return populationSize;
    }

    public int maxRounds() {
        return maxRounds;
    }

    public int maxRoundsWithoutImproving() {
        return maxRoundsWithoutImproving;
    }

    public double mutationProbability() {
        return mutationProbability;
    }


    public Selection selection() {
        return selection;
    }

    public double selectionRate() {
        return selectionRate;
    }

    public Replacement replacement() {
        return replacement;
    }

    public Splitter splitter() {
        return splitter;
    }

    public Mutation[] mutations() {
        return mutations;
    }

    public Crossover[] crossovers() {
        return crossovers;
    }

    public GAParameters populationSize(int populationSize) {
        this.populationSize = populationSize;
        return this;
    }

    public GAParameters maxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
        return this;
    }

    public GAParameters maxRoundsWithoutImproving(int maxRoundsWithoutImproving) {
        this.maxRoundsWithoutImproving = maxRoundsWithoutImproving;
        return this;
    }

    public GAParameters mutationProbability(double mutationProbability) {
        this.mutationProbability = mutationProbability;
        return this;
    }

    public GAParameters selection(Selection selection) {
        this.selection = selection;
        return this;
    }


    public GAParameters selectionRate(double selectionRate) {
        this.selectionRate = selectionRate;
        return this;
    }

    public GAParameters replacement(Replacement replacement) {
        this.replacement = replacement;
        return this;
    }

    public GAParameters splitter(Splitter splitter) {
        this.splitter = splitter;
        return this;
    }

    public GAParameters mutations(Mutation... mutations) {
        this.mutations = mutations;
        return this;
    }

    public GAParameters crossovers(Crossover... crossovers) {
        this.crossovers = crossovers;
        return this;
    }
}
