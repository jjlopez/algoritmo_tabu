package com.dp1mtel.algo.ga.splitter;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.solver.Route;

import java.util.List;

public interface Splitter {
    List<Route> split(Chromosome chromosome);
}
