package com.dp1mtel.algo.ga.splitter;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.model.Customer;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Route;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class StraightSplitter implements Splitter {
    @Override
    public List<Route> split(Chromosome chromosome) {
        Problem problem = chromosome.getProblem();
        List<Route> result = new ArrayList<>(problem.getVehicleCapacity());
        List<Node> nodes = problem.getNodes();
        Route route = chromosome.getRoute();
        double closingTime = problem.getDepot().getClosingTime();
        TDFunction tdFunction = chromosome.getProblem().getTDFunction();
        int capacity = problem.getVehicleCapacity();

        int from = 0;
        while (from < nodes.size()) {
            int to = from;
            double time = 0;
            for (int i = from; i < nodes.size(); i++) {

                time += tdFunction.travelTime(i - from == 0 ? 0 : route.get(i - 1).getNumber(), route.get(i), time);
                Node node = nodes.get(route.get(i).getNumber() - 1);

                double startTime;
                double departureTime;
                boolean validStartTime;
                if (node instanceof Customer) {
                    Customer c = (Customer) node;
                    startTime = Math.max(c.getReadyTime(), time);
                    departureTime = startTime + c.getServiceTime();
                    validStartTime = startTime < c.getDueTime();
                } else {
                    startTime = time;
                    departureTime = startTime;
                    validStartTime = true;
                }
                double depotTravelTime = tdFunction.travelTime(route.get(i), 0, departureTime);;

                if (capacity - node.getDemand() >= 0 && validStartTime && departureTime + depotTravelTime <= closingTime) {
                    time = departureTime;
                    capacity -= node.getDemand();
                    to = i;
                } else {
                    break;
                }
            }
            result.add(route.subRoute(from, to + 1));
            from = to + 1;
        }
        return result;
    }
}
