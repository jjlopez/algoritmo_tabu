package com.dp1mtel.algo.ga;

import com.dp1mtel.algo.ga.splitter.StraightSplitter;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class GASolver implements Solver {
    private final Random random = ThreadLocalRandom.current();
    private GAParameters parameters = new GAParameters();

    public GASolver() {}

    public GASolver(GAParameters parameters) {
        this.parameters = parameters;
    }

    @Override
    public String getName() {
        return "GA_SOLVER";
    }

    @Override
    public Optional<Solution> solve(Problem problem) {
        Population population = PopulationCreator.createPopulation(problem, parameters);
        Chromosome bestEver = null;
        int round = parameters.maxRounds();
        int roundsSinceLastImprovement = 0;
        double bestPopulationFitness = Double.NEGATIVE_INFINITY;

        while (round-- > 0 && roundsSinceLastImprovement < parameters.maxRoundsWithoutImproving()) {
            List<Chromosome> children = new ArrayList<>();
            List<Chromosome> selection = parameters.selection()
                    .select(population, (int) (parameters.selectionRate() * population.getChromosomes().size()))
                    .stream()
                    .map(Chromosome::getChild)
                    .collect(Collectors.toList());

            int length = selection.size();

            for (int i = 0; i < length; i++) {
                int other = random.nextInt(length);
                if (other == i) {
                    other = (other + 1) % length;
                }
                Chromosome child = selection.get(i).cross(selection.get(other));
                if (random.nextDouble() <= parameters.mutationProbability()) {
                    child.mutate();
                }
                children.add(child);
            }

            parameters.replacement().replace(population, children);
            Chromosome best = population.getBestChromosome().orElse(null);

            double populationFitness = population.getChromosomes().stream().mapToDouble(Chromosome::fitness).sum();

            if (best != null && (bestEver == null || bestEver.fitness() < best.fitness())) {
                bestEver = best;
            }

            if (populationFitness > bestPopulationFitness) {
                bestPopulationFitness = populationFitness;
                roundsSinceLastImprovement = 0;
            } else {
                roundsSinceLastImprovement++;
            }

        }

        if (bestEver == null) {
            return Optional.empty();
        } else {
            return Optional.of(new Solution(problem, bestEver.routes(new StraightSplitter())));
        }
    }
}
