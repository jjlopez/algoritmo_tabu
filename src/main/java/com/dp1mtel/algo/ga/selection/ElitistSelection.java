package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class ElitistSelection implements Selection {

    @Override
    public List<Chromosome> select(Population population, int selectionSize) {
        SortedMap<Double, Chromosome> map = new TreeMap<>();
        population.getChromosomes().forEach((c) -> map.put(1.0 / c.fitness(), c));
        List<Chromosome> selectedPopulation = new ArrayList<>(selectionSize);
        for (Chromosome chromosome : map.values()) {
            selectedPopulation.add(chromosome);
            if (--selectionSize == 0) {
                break;
            }
        }
        return selectedPopulation;
    }
}
