package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RouletteSelection implements Selection {
    private Random random = ThreadLocalRandom.current();

    @Override
    public List<Chromosome> select(Population population, int selectionSize) {
        List<Chromosome> result = new ArrayList<>();
        List<Chromosome> candidates = new ArrayList<>(population.getChromosomes());

        while (result.size() < selectionSize) {
            double totalFitness = candidates.stream().mapToDouble(Chromosome::fitness).sum();
            double ball = random.nextDouble() * totalFitness;
            double pos = 0;
            Chromosome winner = null;

            for (Chromosome c : candidates) {
                pos += c.fitness();
                if (ball < pos) {
                    winner = c;
                    break;
                }
            }

            if (winner != null) {
                result.add(winner);
                candidates.remove(winner);
            }
        }
        return result;
    }
}
