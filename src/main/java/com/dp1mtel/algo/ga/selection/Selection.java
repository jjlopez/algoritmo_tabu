package com.dp1mtel.algo.ga.selection;

import com.dp1mtel.algo.ga.Chromosome;
import com.dp1mtel.algo.ga.Population;

import java.util.List;

public interface Selection {
    List<Chromosome> select(Population population, int selectionSize);
}
