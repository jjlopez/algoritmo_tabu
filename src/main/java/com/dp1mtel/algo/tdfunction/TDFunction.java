package com.dp1mtel.algo.tdfunction;

import com.dp1mtel.algo.model.Numberable;

@FunctionalInterface
public interface TDFunction {
    double travelTime(int from, int to, double startTime);

    default double travelTime(Numberable from, Numberable to, double startTime) {
        return travelTime(from.getNumber(), to.getNumber(), startTime);
    }

    default double travelTime(Numberable from, int to, double startTime) {
        return travelTime(from.getNumber(), to, startTime);
    }

    default  double travelTime(int from, Numberable to, double startTime) {
        return travelTime(from, to.getNumber(), startTime);
    }
}
