package com.dp1mtel.algo.tdfunction;

import com.dp1mtel.algo.model.Problem;

public interface TDFunctionFactory {

    /**
     * Get the name of the factory
     *
     * @return the name
     */
    String getName();

    /**
     * Create a TDFunction based on a problem
     *
     * @return the tdFunction
     */
    TDFunction createTDFunction(Problem problem);
}
