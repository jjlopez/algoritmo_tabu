package com.dp1mtel.algo.tabu;

import com.dp1mtel.algo.InsertionHeuristics;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.solver.strategy.SimpleSolutionLocator;
import com.dp1mtel.algo.solver.strategy.SolutionLocator;
import com.dp1mtel.algo.solver.Solution;
import com.dp1mtel.algo.solver.Solver;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.List;
import java.util.Optional;

public class TabuSolver implements Solver {

    private long executionTime;
    private long currentIteration;
    private long iterationsNoImprovement;
    private TabuParameters params;
    private SolutionLocator solutionLocator;
    private CircularFifoQueue<Solution> tabuList;
    private long startTime;

    public TabuSolver(SimpleSolutionLocator solutionLocator, TabuParameters params) {
        setSolutionLocator(solutionLocator);
        setParams(params);
    }

    @Override
    public String getName() {
        return "TABU_SOLVER";
    }

    @Override
    public Optional<Solution> solve(Problem problem) {
        Solution initialSolution = buildInitialSolution(problem);

        Solution bestSolution = initialSolution;
        Solution currentSolution = initialSolution;

        this.executionTime = 0;
        this.currentIteration = 0;
        tabuList = new CircularFifoQueue<>(params.getTabuListSize());
        this.startTime = System.currentTimeMillis();

        while (!hasReachedStopCondition()) {
            List<Solution> candidates = solutionLocator.findNeighbours(currentSolution);

            Solution bestNeighbour = solutionLocator.findBestNeighbour(
                    candidates,
                    IteratorUtils.toList(tabuList.iterator()));

            if (bestSolution.getCost() < bestNeighbour.getCost()) {
                bestSolution = bestNeighbour;
            } else {
                iterationsNoImprovement++;
            }

            tabuList.add(currentSolution);
            currentSolution = bestNeighbour;

            this.currentIteration++;
            this.executionTime = System.currentTimeMillis() - startTime;
        }

        return Optional.of(bestSolution);
    }

    private Solution buildInitialSolution(Problem problem) {
        Solver solver = new InsertionHeuristics();
        return solver.solve(problem).get();
    }

    public boolean hasReachedStopCondition() {
        return executionTime >= params.getMaxExecutionTime() ||
                currentIteration >= params.getMaxIterations() ||
                iterationsNoImprovement >= params.getMaxIterationsNoImprovement();
    }

    public TabuParameters getParams() {
        return params;
    }

    public void setParams(TabuParameters params) {
        this.params = params;
    }

    public void setSolutionLocator(SolutionLocator solutionLocator) {
        this.solutionLocator = solutionLocator;
    }

}
