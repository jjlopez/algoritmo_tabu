package com.dp1mtel.algo.tabu;

public class TabuParameters {
    private long maxIterations;
    private long maxIterationsNoImprovement;
    private long maxExecutionTime;
    private int tabuListSize;

    public TabuParameters(long maxIterations,
                          long maxIterationsNoImprovement,
                          long maxExecutionTime,
                          int tabuListSize) {
        setMaxIterations(maxIterations);
        setMaxExecutionTime(maxExecutionTime);
        setMaxIterationsNoImprovement(maxIterationsNoImprovement);
        setTabuListSize(tabuListSize);
    }

    public long getMaxExecutionTime() {
        return maxExecutionTime;
    }

    public void setMaxIterations(long maxIterations) {
        this.maxIterations = maxIterations;
    }

    public long getMaxIterationsNoImprovement() {
        return maxIterationsNoImprovement;
    }

    public void setMaxIterationsNoImprovement(long maxIterationsNoImprovement) {
        this.maxIterationsNoImprovement = maxIterationsNoImprovement;
    }

    public long getMaxIterations() {
        return maxIterations;
    }

    public void setMaxExecutionTime(long maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }

    public int getTabuListSize() {
        return tabuListSize;
    }

    public void setTabuListSize(int tabuListSize) {
        if (tabuListSize < 1) {
            throw new IllegalArgumentException("TabuSolver list size must be positive");
        }
        this.tabuListSize = tabuListSize;
    }
}
