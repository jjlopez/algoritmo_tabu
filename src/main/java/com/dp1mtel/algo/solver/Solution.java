package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.model.Customer;
import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.model.Problem;
import com.dp1mtel.algo.tdfunction.TDFunction;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    private final Problem problem;
    private final List<Route> routes;
    private final TDFunction tdFunction;

    public Solution(Problem problem, List<Route> routes) {
        this.problem = problem;
        this.tdFunction = problem.getTDFunction();
        this.routes = routes;
    }

    public Solution(Solution src) {
        this(src.problem, new ArrayList<>(src.routes));
    }

    public Problem getProblem() {
        return problem;
    }

    public List<Route> getRoutes() {
        return Collections.unmodifiableList(routes);
    }

    public boolean isValid() {
        Set<Customer> customers = new HashSet<>(problem.getCustomers());
        if (getRoutes().size() > problem.getAvailableVehicles()) {
            return false;
        }

        for (Route route : getRoutes()) {
            if (route.getNodes().stream().mapToDouble(Node::getDemand).sum() > problem.getVehicleCapacity()) {
                return false;
            }

            List<Customer> routeCustomers = route.getNodes().stream()
                    .filter((node) -> node instanceof Customer)
                    .map((node) -> (Customer) node)
                    .collect(Collectors.toList());
            if (!customers.containsAll(routeCustomers)) {
                return false;
            }
            routeCustomers.forEach(customers::remove);
        }

        return customers.isEmpty();
    }

    public double getCost() {
        List<int[]> rls = routes.stream()
                .map(Route::toIntArr)
                .collect(Collectors.toList());
        return problem.objectiveFunction(rls);
    }
}
