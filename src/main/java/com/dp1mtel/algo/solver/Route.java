package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.model.Node;
import com.dp1mtel.algo.model.Problem;

import java.util.*;

public class Route {
    private final List<Node> route;
    private final HashSet<Node> nodes;

    public Route(List<? extends Node> nodes) {
        this.nodes = new HashSet<>(nodes);
        this.route = new ArrayList<>(nodes);
    }

    public Route(Route src) {
        this(src.getRoute());
    }

    public Set<Node> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }
    public List<Node> getRoute() {
        return Collections.unmodifiableList(route);
    }

    public int[] toIntArr() {
        return nodes.stream().mapToInt(Node::getNumber).toArray();
    }

    public Node get(int index) {
        if (index < 0 || index > getNodes().size()) {
            throw new IllegalArgumentException("Invalid index");
        }
        return route.get(index);
    }

    public int size() {
        return route.size();
    }

    public Optional<Node> getNodeById(int id) {
        return nodes.stream()
                .filter((node) -> node.getNumber() == id)
                .findFirst();
    }

    public void moveNode(int indA, int indB) {
        if (indA <= 0 || indB <= 0 || indA >= route.size() - 2 || indB >= route.size() - 2) {
            throw new IllegalArgumentException("Invalid index to swap");
        }
        Node n = this.route.remove(indA);
        if (indA <= indB) {
            this.route.add(indB, n);
        } else {
            this.route.add(indB + 1, n);
        }
    }

    public void add(int index, Node... nodes) {
        List<Node> newNodes = Arrays.asList(nodes);
        this.route.addAll(index, newNodes);
        this.nodes.addAll(newNodes);
    }

    public void removeNode(int index) {
        // Check bounds and do not remove depots
        if (index <= 0 || index >= route.size() - 1) {
            throw new IllegalArgumentException("Invalid index to remove");
        }
        Node n = this.route.get(index);
        this.route.remove(index);
        this.nodes.remove(n);
    }

    public boolean isEmpty() {
        return this.route.isEmpty();
    }

    public double demand() {
        return route.stream().mapToDouble(Node::getDemand).sum();
    }

    /**
     * Swap operation (mainly for 2-opt swap
     *
     * @param indA Starting index
     * @param indB End index
     */
    public void swap(int indA, int indB) {
        if (indA <= 0 || indB <= 0 || indA >= route.size() - 1 || indB >= route.size() - 1) {
            throw new IllegalArgumentException("Invalid index to swap");
        }
        Collections.reverse(route.subList(indA, indB + 1));
    }

    public Route subRoute(int indA, int indB) {
        if (indA <= 0 || indB <= 0 || indA >= route.size() - 1 || indB >= route.size() - 1) {
            throw new IllegalArgumentException("Invalid index to swap");
        }

        return new Route(route.subList(indA, indB));
    }

    public void set(int index, Node node) {
        if (index <= 0 || index >= route.size() - 1) {
            throw new IllegalArgumentException("Invalid insertion index");
        }
        Node old = route.get(index);
        route.set(index, node);
        nodes.remove(old);
        nodes.add(node);
    }
}
