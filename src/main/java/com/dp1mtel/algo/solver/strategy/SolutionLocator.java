package com.dp1mtel.algo.solver.strategy;

import com.dp1mtel.algo.solver.Solution;

import java.util.Collections;
import java.util.List;

public interface SolutionLocator {
    /**
     * Find a solution's neighbourhood
     *
     * @param solution The current solution
     * @return list of solutions
     */
    List<Solution> findNeighbours(Solution solution);

    /**
     * Find a solution's best neighbour (candidates) that are not tabu
     *
     * @param candidates Best neighbour candidates
     * @param tabuSolutions Tabu solutions to avoid
     * @return the best solution
     */
    Solution findBestNeighbour(List<Solution> candidates, List<Solution> tabuSolutions);

    default Solution findBestNeighbour(List<Solution> candidates) {
        return findBestNeighbour(candidates, Collections.emptyList());
    }
}
