package com.dp1mtel.algo.solver;

import com.dp1mtel.algo.tdfunction.TDFunction;
import com.dp1mtel.algo.model.Problem;

import java.util.Optional;

public interface Solver {

    /**
     * Get the name of the solver
     *
     * @return the name
     */
    String getName();

    /**
     * Solve the problem with the time-dependent function
     *
     * @param problem The problem to solve
     * @param tdFunction The time-dependent function
     * @return the solution
     */
    Optional<Solution> solve(Problem problem);
}
